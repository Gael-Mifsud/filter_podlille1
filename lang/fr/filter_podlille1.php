<?php // $Id$
// Chaînes FR pour filter/PodLille1.

$string['filtername']	= "Conversion des URLs PodLille1";
$string['url']			= "URL";
$string['url_desc']		= "Le filtre remplacera l'url indiquée ici dans tout le contenu de Moodle, par une iframe adaptée à l'outil PodLille1.";
$string['url_help']		= "Le filtre remplacera l'url indiquée ici dans tout le contenu de Moodle, par une iframe adaptée à l'outil PodLille1.";
$string['size']			= "Qualité de la vidéo";
$string['size_desc']	= "Qualité de la vidéo : 240, 480 ou 720.";
$string['size_help']	= "Qualité de la vidéo : 240, 480 ou 720.<br> (valeur par défaut = 480).";
$string['width']		= "Largeur";
$string['width_desc']	= "Largeur en px de l'iframe contenant la vidéo.";
$string['width_help']	= "Largeur en px de l'iframe contenant la vidéo.<br> (valeur par défaut = 854).";
$string['height']       = "Hauteur";
$string['height_desc']  = "Hauteur en px de l'iframe contenant la vidéo.";
$string['height_help']  = "Hauteur en px de l'iframe contenant la vidéo.<br> (valeur par défaut = 480).";

