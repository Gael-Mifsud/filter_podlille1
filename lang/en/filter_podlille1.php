<?php // $Id$
// Language string for filter/PodLille1.

$string['filtername']	= 'PodLille1 URLs';
$string['url']          = "URL";
$string['url_desc']     = "The filter will replace this url in all the Moodle content.";
$string['url_help']     = "The filter will replace this url in all the Moodle content.";
$string['size']         = "Size for the iframe";
$string['size_desc']    = "Video quality.";
$string['size_help']    = "Video quality : 240, 480 or 720 (default value = 480).";
$string['width']        = "Width";
$string['width_desc']   = "Width in px of the iframe with the video (default value = 854).";
$string['width_help']   = "Width in px of the iframe with the video.<br> (default value = 854).";
$string['height']       = "Height";
$string['height_desc']  = "Height in px of the iframe with the video (default value = 480).";
$string['height_help']  = "Height in px of the iframe with the video.<br> (default value = 480).";

